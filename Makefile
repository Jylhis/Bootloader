##
# OS
#
# @file
# @version 0.1

ASM = ../i686-elf-4.9.1-Linux-x86_64/bin/i686-elf-as
CC = ../i686-elf-4.9.1-Linux-x86_64/bin/i686-elf-gcc

# Builds all 
all:
	${ASM} boot.s -o boot.o
	${CC} -c kernel.c -o kernel.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
	${CC} -T linker.ld -o os.bin -ffreestanding -O2 -nostdlib boot.o kernel.o -lgcc

# Generates iso image which can be booted
create-grub:
	mkdir -p isodir/boot/grub
	cp os.bin isodir/boot/os.bin
	cp grub.cfg isodir/boot/grub/grub.cfg
	grub2-mkrescue -o os.iso isodir
	rm -r isodir

# Runs iso file
run-grub:
	qemu-system-i386 -cdrom os.iso

# Runs kernel directly
run:
	qemu-system-i386 -kernel os.bin

# end
